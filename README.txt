This project wants to make a tool that converts an ontology description file (OWL) into an Orocos component.

author: Fabrizio Boriero <fabrizio.boriero@gmail.com>

Based on the "orocreate-pkg" script (www.orocos.org).


== 24/07/2013 ==
The project is very messy now, is possible to test it with:

mkdir test
cd test
../oromodel ../xml_examples/robot.xml nameComponent

and check the results into src/nameComponent-component.hpp
