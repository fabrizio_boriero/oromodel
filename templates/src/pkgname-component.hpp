#ifndef OROCOS_@PKGNAME@_COMPONENT_HPP
#define OROCOS_@PKGNAME@_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <vector>

using namespace RTT;
using namespace std;

class @Pkgname@ : public RTT::TaskContext{
  public:
    @Pkgname@(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();


 protected:
@InputPortsList@
@OutputPortsList@
@PropertiesList@
};
#endif
